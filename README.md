# People - Remote Users Management

* [Usages](https://github.com/konforti/people/wiki/Usages)
* [Installation](https://github.com/konforti/people/wiki/Installation)
* [Users & Roles](https://github.com/konforti/people/wiki/Users-&-Roles)
* [API](https://github.com/konforti/people/wiki/API)
* [Login workflow](https://github.com/konforti/people/wiki/Login-workflow)
* [People.js] (https://github.com/konforti/people/wiki/People.js)
* [Hooks & Rules](https://github.com/konforti/people/wiki/Hooks-&-Rules)

## Live demo

| Platform                                 | Username | Password |
| ---------------------------------------- | -------- | -------- |
|https://people-razkonforti.rhcloud.com/   | admin    | rrg2st   |


# Usages
**People** is a simple, easy to use, stand alone remote users management system.
Yet, complete, powerful & secure following best practice & best security. 

Never configure and implement a user management system again.
Install **People** once, and you got all that you need.

_Advantages:_
* Use it with any system or language
* Easy to accomplish multi-device system
* Move your entire users system between sites or apps
* Rewrite your system with no concerns

# Features
### Admin UI Features:
_Clear and intuitive full control admin panel_
* Keep and manage user identities
* Assign roles to a user
* Assign permissions to roles
* Block user
* Set user status
* Post notes on a user
* User extra fields

### People.js:
_Full features flow with one line._
* User login
* User register
* Social login
* Welcome email
* Email verification
* Forgot password workflow
* Reset password
* Logout
* User block
* Profile block

### API:
_A post-session server side API to get full control on your users._
* Authenticate and secure REST API
* Retrieve and update users
* Retrieve and update current logged in user
* Retrieve and update roles and permissions
* JSON will be returned in all responses from the API
